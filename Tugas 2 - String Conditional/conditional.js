//soal 1
console.log("Soal 1 : If-else \n")

var nama = "John"
var peran = ""


if (nama == '' && peran == '') {
	console.log("Nama harus diisi!")
} else if (nama == 'John' && peran == '') {
	console.log("Halo"+ " " + nama + ", Pilih peranmu untuk memulai game!" )
} else if (nama == 'Jane' && peran == 'Penyihir') {
	console.log("Selamat datang di Dunian Werewolf," + " " + nama + "\nHalo Penyihir"+ " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf" )
} else if (nama == 'Jenita' && peran == 'Guard') {
	console.log("Selamat datang di Dunia Werewolf,"+ " " + nama + "\nHalo"+" "+ peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
	console.log("Selamat datang di Dunia Werewolf,"+" "+nama+"\nHalo"+ " " + peran + " "+ nama+ ", kamu akan memakan mangsa setiap malam!")
}

// soal 2
console.log("\nSoal 2 : Switch Case\n")

var tanggal = 11; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 1; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2004; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var teksBulan;

switch (true) {
	case (tanggal < 1 || tanggal > 31): {
		console.log('Input tanggal salah')
		break;
	}
	case (tahun < 1900 || tahun >2200): {
		console.log('Input Tahun salah')
		break;
	}
	case (bulan > 12 || bulan < 1):
	console.log('Input Bulan salah')
	break;
default:
{
	switch (true) {
		case bulan == 1:
		 teksBulan = 'Januari';
		 break;
		case bulan == 2:
		 teksBulan = 'Februari';
		 break;
		case bulan == 3:
		 teksBulan = 'Maret';
		 break;
		case bulan == 4:
		 teksBulan = 'April';
		 break;
		case bulan == 5:
		 teksBulan = 'Mei';
		 break;
		case bulan == 6:
		 teksBulan = 'Juni';
		 break;
		case bulan == 7:
		 teksBulan = 'Juli';
		 break;
		case bulan == 8:
		 teksBulan = 'Agustus';
		 break;
		case bulan == 9:
		 teksBulan = 'September';
		 break;
		case bulan == 10:
		 teksBulan = 'Oktober';
		 break;
		case bulan == 11:
		 teksBulan = 'November';
		 break;
		case bulan == 12:
		 teksBulan = 'Desember';
		 break;   
	}

}

}

console.log(tanggal + " " + teksBulan + " " + tahun)

