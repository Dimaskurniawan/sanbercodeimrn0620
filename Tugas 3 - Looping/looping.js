// Soal 1
console.log('No. 1 Looping While\n')
console.log('LOOPING PERTAMA')

var Nomor = 2;
while (Nomor <= 20) {
	console.log(Nomor+" - I love coding")
	Nomor += 2;
}

console.log('LOOPING KEDUA')

var Nomor2 = 20;
while(Nomor2 >=2) {
	console.log(Nomor2 + " - I will become a mobile developer")
	Nomor2 -= 2;	
}

// Soal 2
console.log('\nNo. 2 Looping menggunakan for\n')
for (var Nomor3 = 1; Nomor3 <= 20; Nomor3++ ){
 if (Nomor3 % 3 === 0 && Nomor3 % 2 === 1) {
		console.log(Nomor3 + " - I Love Coding")
	} else if  (Nomor3 % 2 === 1) {
		console.log(Nomor3 + " - Santai")
	} else if (Nomor3 % 2 === 0) {
		console.log(Nomor3 + " - Berkualitas")
	} 
	
}

// Soal 3
console.log("\nNo. 3 Membuat Persegi Panjang\n")
for (var Persegi = 1; Persegi <= 4; Persegi++) {
	console.log("########")
	}

// Soal 4
console.log("\nNo. 4 Membuat Tangga\n")
var s = "";

for (i = 1; i <= 7; i++) {
  for (j = 1; j <= i; j++) {
    s += "#";
  }
  s += "\n";
}

console.log(s);

console.log(" \n No. 5 Membuat Papan Catur");

var z = "";
var kolom = 8;
var baris = 8;
var x = 1;
while (x <= baris) {
  var y = 1;
  while (y <= kolom) {
    if ((x + y) % 2 > 0) {
      z += "#";
    } else {
      z += " ";
    }
    y++;
  }
  x++;
  z += "\n";

}
console.log(z);
	